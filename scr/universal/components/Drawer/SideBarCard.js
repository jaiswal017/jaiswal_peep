import React, { Component } from "react";

import { Text, TouchableOpacity } from "react-native";

import Icon from "react-native-vector-icons/FontAwesome";

export const Card = props => (
  <TouchableOpacity
    onPress={props.onPress}
    style={{
      flexDirection: "row",
      margin: 10,
      justifyContent:'space-around',
      alignItems:'center',
      borderBottomColor:'#E7E7E7',
      borderBottomWidth:1
    }}
  >
     <Icon
          name={props.iconName}
          color={'#00cccc'}
          size={25}
            />
    <Text style={styles.text}>{props.title}</Text>
  </TouchableOpacity>
);

const styles = {
  text: {
    fontSize: 18,
    marginLeft: 20,
    marginTop: 10,
    justifyContent: "center",
    color: "black",
    width: 200,
    fontWeight: "500"
  }
};
