import React, { Component } from "react";
import {TouchableOpacity , Alert} from 'react-native'
import { Header, Left, Body, Right, Button, Icon, Title } from "native-base";

export default class Headers extends Component {
  render() {
    const {navigate} = this.props.navigation
    return (
      <Header style={{ backgroundColor: '#fff', height: 55 }}>
        <Left>
          <Button transparent  onPress={this.props.onOpen}>
            <Icon
              name="align-left"
              style={{ color: "#00cccc" }}
              type="FontAwesome"
              fontSize={25}
            />
          </Button>
        </Left>
        <Body>
          <Title style={{ color: "#00cccc", fontWeight: 'bold', fontSize: 20 }}>Dashboard</Title>
        </Body>
        <Right>
          <TouchableOpacity onPress={() => Alert.alert('bell Demo')} >
              <Icon
                   name='bell'
                   type={'FontAwesome'}
                   style={{ color: '#cca300' }}
                   fontSize={25}
                    />
                </TouchableOpacity> 
    </Right>
      </Header>
    );
  }
}
