import React, { Component } from "react";
import { Image, ImageBackground, View, Text } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";

export const DrawerHeader = (props) => {
  return (
    <View style={styles.topVIew}>
      <Text style={styles.titleAlign}>PEEP</Text>
      <View style = {{justifyContent:'space-between',flexDirection:'row',  marginEnd:30}}>
       <View>
      <Text style={[styles.nameText,{fontWeight:'bold'}]}>John Deo</Text>
      <Text style={[styles.nameText,{ fontSize:20}]}>Metro Hospital</Text>
      </View>
      <Image
        style={{height:60, width:70}}
        source ={require('../../../asset/profile.jpg')}/>
      </View>
    </View>
  );
};

const styles = {
  topVIew: {
    width: "100%",
    height: 160,
    backgroundColor: '#fff',
    marginBottom: 10, 
    borderBottomWidth:1,
    borderColor:'gray'
  },
  titleAlign: {
    marginStart: 20,
    marginTop: 25,
    marginBottom: 10,
    color: "#00cccc",
    fontWeight: "bold",
    fontSize: 25
  },
  nameText: {
    marginLeft: 25,
    marginTop: 5,
    color: "#00cccc",
    fontSize: 25,

  },
  image: {
    width: 70,
    height: 65,
    marginLeft: 25,
    marginTop: 20,
    borderRadius: 25,
    marginBottom: 20
  }
};
