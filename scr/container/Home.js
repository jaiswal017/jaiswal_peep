import React, { Component } from "react";
import { View, Text, TouchableOpacity, ImageBackground , ScrollView} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { H1, H2, H3 } from "native-base";
import { message } from "../constants/string";
import Drawer from '../universal/components/Drawer/Drawer'
import * as Progress from 'react-native-progress';

class Home extends Component {
  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={{ flex: 1, backgroundColor:'#fff' }}>
      <Drawer
      navigation = {this.props.navigation}>

     <H1 style={styles.titleFirst}>PEEP</H1>
     <ScrollView contentContainerStyle={styles.contentContainer}>

     <View style = {{backgroundColor:'#00cccc', flex:1,alignItems:'center', paddingBottom:10}}>
     <Text style={styles.subTitle}>Welcome John Deo</Text>
          <Text style={styles.titleAlign}>Let's Start</Text>
          <Text style={[styles.titleAlign,{fontSize:25}]}>With Basic</Text>
          <View style = {{borderWidth:0.5, borderColor:'#fff',width:'50%'}}></View>
          <Text style={styles.subTitle}>Your Score Card</Text>
          <Progress.Pie 
          color={'#ffbf00'}
          progress={0.3} size={50} />
          <Text style = {{fontSize:30, color:'#ffbf00',margin:10}}>30%</Text>
            <Text style = {styles.textDetails}>{message.PLEASE_GO_THROUGH_THE_BELOW}</Text>
           
            <TouchableOpacity 
            onPress = {()=>navigate('BaseCareEducation')}
            style = {styles.cardView}>
            <Text style={[styles.titleAlign,{ color:"#00cccc", fontSize:30}]}>Basic Care Education</Text>
            <Progress.Bar 
            color = {'#ffbf00'}
            progress={0.3} width={250} />
                </TouchableOpacity>
                
          </View>
          </ScrollView>
     </Drawer>
      </View>
    );
  }
}

const styles = {
  parentView: {
    alignItems: "center",
    marginTop: 10,
    flex: 1,
    margin: 10,
    justifyContent:'space-between'
  },
  contentContainer: {
    paddingVertical: 20
  },
  cardView:{
    width:'90%',
    borderRadius:10,
    alignItems:'center',
    justifyContent:'space-around',
    height:120,
    marginTop:10,
    backgroundColor:'#fff'
  },
  textDetails:{
      color:"#EEE", 
      fontSize:20, 
      textAlign:'center',
      margin:20},
  signView:{
      borderBottomWidth:1, 
      marginLeft:5,
      marginRight:5,
    borderColor:"gray"},
  button: {
    alignItems: "center",
    backgroundColor: "#cca300",
    borderRadius: 20,
    margin: 20,
    paddingLeft: 20,
    paddingRight: 20
  },
  buttonText: {
    fontSize: 18,
    color: "white",
    margin: 12
  },
  subTitle: {
    fontSize: 18,
    marginTop:10,
    textAlign: "center",
    color:'#EEE'
  },
  titleAlign: {
    textAlign: "center",
    margin: 8,
    color: "#fff",
    fontWeight: "bold",
    fontSize: 35
  },
  titleFirst: {
    fontSize: 30,
    textAlign:'center',
    marginTop: 20
  }
};

const mapStateToProps = state => {
  return {
   // reduxData: state.todos.data
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
     // addTodo: addTodo
    },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(Home);
