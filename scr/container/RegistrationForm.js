import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  Picker
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { H1, H2, H3, Item, Label, Input, Form, Content } from "native-base";
import { message } from "../constants/string";
import GenericHeader from "../universal/components/GenericHeader";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

class RegistrationForm extends Component {
  state = {
    language: ""
  };
  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={{ backgroundColor: "#fff" }}>
        <KeyboardAwareScrollView
          style={{ backgroundColor: "#fff" }}
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={true}
        >
          <GenericHeader
            navigation={this.props.navigation}
            headerTitle={"PEEP"}
          />

          <Text style={styles.titleAlign}>{message.REGISTRATION}</Text>
          <Text style={styles.subTitle}>{message.BETTER_CARE_STARTS_WITH}</Text>
          <Text style={styles.messageText}>
            {message.THANK_Q_FOR_CHOOSING_PEEP}
          </Text>

          <Text style={styles.subTitle}>{message.PERSONAL_INFORMATION}</Text>
          {this.textArea("Email-address*", "email-address", null)}
          {this.textArea("Last Name, First Name*", "numeric", null)}
          {this.textArea("Phone Number*", "numeric", 10)}
          {this.textArea("Social Security Number*", "numeric", null)}
          {this.textArea("Year of Birth*", "numeric", null)}
          <View style={styles.pickerView}>
            <TextInput
              style={[styles.textInput, { width: "45%", marginStart: 0 }]}
              placeholder={"Zip Code"}
              keyboardType={"numeric"}
              maxLength={6}
            />

            <Picker
              selectedValue={this.state.language}
              mode={"dropdown"}
              style={{
                borderBottomWidth: 1,
                width: "45%",
                borderBottomColor: "gray"
              }}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({ language: itemValue })
              }
            >
              <Picker.Item label="Country of stay " value="Country" />
              <Picker.Item label="India" value="India" />
              <Picker.Item label="America" value="America" />
            </Picker>
          </View>

          <View style={styles.pickerView}>
            <Picker
              selectedValue={this.state.language}
              mode={"dropdown"}
              style={{
                borderBottomWidth: 1,
                width: "45%",
                borderBottomColor: "gray"
              }}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({ language: itemValue })
              }
            >
              <Picker.Item label="Gender" value="Gender" />
              <Picker.Item label="Male" value="Male" />
              <Picker.Item label="Female" value="Female" />
            </Picker>

            <Picker
              selectedValue={this.state.language}
              mode={"dropdown"}
              style={{
                borderBottomWidth: 1,
                width: "45%",
                borderBottomColor: "gray"
              }}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({ language: itemValue })
              }
            >
              <Picker.Item label="language" value="language" />
              <Picker.Item label="English" value="English" />
              <Picker.Item label="Hindi" value="js" />
            </Picker>
          </View>
          <TouchableOpacity 
          onPress = {()=> navigate('Home')}
          style={styles.button}>
            <Text style={styles.buttonText}>{message.REGISTRATION}</Text>
          </TouchableOpacity>
        </KeyboardAwareScrollView>
      </View>
    );
  }

  textArea = (placeholder, type, maxLength) => {
    return (
      <TextInput
        style={styles.textInput}
        keyboardType={type}
        placeholder={placeholder}
        maxLength={maxLength}
      />
    );
  };
}

const styles = {
  pickerView: {
    flexDirection: "row",
    marginStart: 15,
    marginTop: 5,
    justifyContent: "space-between"
  },
  button: {
    alignItems: "center",
    backgroundColor: "#cca300",
    borderRadius: 20,
    margin: 20,
    paddingLeft: 20,
    paddingRight: 20
  },
  buttonText: {
    fontSize: 18,
    color: "white",
    margin: 12
  },

  textInput: {
    padding: 10,
    marginStart: 15,
    fontSize: 15,
    marginEnd: 15
  },
  titleAlign: {
    marginStart: 15,
    marginTop: 15,
    marginBottom: 15,
    color: "#00cccc",
    fontWeight: "bold",
    fontSize: 25
  },
  subTitle: {
    fontSize: 20,
    marginStart: 15,
    color: "#5c5c3d",
    fontWeight: "bold",
    marginBottom: 5
  },
  messageText: {
    fontSize: 17,
    marginStart: 15,
    marginBottom: 25,
    marginEnd: 15
  }
};

const mapStateToProps = state => {
  return {
    //  reduxData: state.todos.data
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      // addTodo: addTodo
    },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(RegistrationForm);
