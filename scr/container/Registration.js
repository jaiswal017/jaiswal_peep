import React, { Component } from "react";
import { View, Text, TouchableOpacity, ImageBackground } from "react-native";
import { addTodo } from "../action/action";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { H1, H2, H3 } from "native-base";
import { message } from "../constants/string";

class Registration extends Component {
  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={{ flex: 1, backgroundColor:'#fff' }}>
        <View style={styles.parentView}>
          <H1 style={styles.titleFirst}>PEEP</H1>
          <Text style={styles.titleAlign}>{message.PATIENT_EMPOWERMENT}</Text>
          <Text style={styles.subTitle}>{message.HELPING_YOU_PREVENT}</Text>
          <TouchableOpacity 
          onPress = {()=> navigate("RegistrationForm")}
          style={styles.button}>
            <Text style={styles.buttonText}>{message.REGISTRATION}</Text>
          </TouchableOpacity>

          <View style={{ flexDirection: "row" }}>
            <Text style={styles.subTitle}>
              {message.ALREADY_HAVE_AN_ACCOUNT}
            </Text>
            <TouchableOpacity style = {styles.signView}>
              <Text style={styles.subTitle}>Sign In</Text>
            </TouchableOpacity>
            <Text style={styles.subTitle}>here</Text>
          </View>
        </View>
        <ImageBackground
          style={{ flex: 1 }}
          source={require("../asset/register.jpeg")}
        />
      </View>
    );
  }
}

const styles = {
  parentView: {
    alignItems: "center",
    marginTop: 10,
    flex: 1,
    margin: 10,
    justifyContent:'space-between'
  },
  signView:{
      borderBottomWidth:1, 
      marginLeft:5,
      marginRight:5,
    borderColor:"gray"},
  button: {
    alignItems: "center",
    backgroundColor: "#cca300",
    borderRadius: 20,
    margin: 20,
    paddingLeft: 20,
    paddingRight: 20
  },
  buttonText: {
    fontSize: 18,
    color: "white",
    margin: 12
  },
  subTitle: {
    fontSize: 20,
    textAlign: "center"
  },
  titleAlign: {
    textAlign: "center",
    margin: 8,
    color: "#00cccc",
    fontWeight: "bold",
    fontSize: 30
  },
  titleFirst: {
    fontSize: 30,
    marginTop: 20
  }
};

const mapStateToProps = state => {
  return {
    reduxData: state.todos.data
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      addTodo: addTodo
    },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(Registration);
