import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  Picker
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { H1, H2, H3, Item, Label, Input, Form, Content } from "native-base";
import { message } from "../constants/string";
import GenericHeader from "../universal/components/GenericHeader";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import * as Progress from 'react-native-progress';

class BaseCareEducation extends Component {
  state = {
    language: ""
  };
  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={{ backgroundColor: "#fff" , flex:1}}>
          <GenericHeader
            navigation={this.props.navigation}
            headerTitle={"Care"}
          />
        <View style={{ backgroundColor: "#00cccc", height:"40%",alignItems:'center'  }}>
        <Text style={[styles.titleAlign,{ color:"#fff", fontSize:30}]}>Basic Care Education</Text>
        <Text style = {{fontSize:15, margin:5, color:'#fff'}}> 7 Activity   21 Question</Text>
            <Progress.Bar 
            color = {'#ffbf00'}
            progress={0.3} width={250} />
          </View>

            <TouchableOpacity style = {styles.cardView}>
            <Text style = {{fontSize:15}}>Activity 1 - completed</Text>
            <Text style = {{fontSize:20, color:'#000'}}>{message.INTRODUCE_AND_VIRTUAL_TOUR_OF_THE_PROGRAMMER}</Text>
            <Text style = {{fontSize:15, color:'#00cccc'}}>1 video watched</Text>
            <Text style = {{fontSize:15, color:'#00cccc'}}>4 Question Answered</Text>
            <TouchableOpacity 
             style={styles.button}>
            <Text style={styles.buttonText}>See the Activity</Text>
          </TouchableOpacity>
                </TouchableOpacity>
      </View>
    );
  
  };
}

const styles = {
  pickerView: {
    flexDirection: "row",
    marginStart: 15,
    marginTop: 5,
    justifyContent: "space-between"
  },
  cardView:{
    margin:20,
    padding:20,
    borderRadius:10,
    justifyContent:'space-around',
    borderColor:'#00cccc',
    borderWidth:1,
    marginTop:10,
    backgroundColor:'#fff',
    top:-100
  },
  button: {
    alignItems: "center",
    backgroundColor: "#00cccc",
    borderRadius: 20,
    margin: 20,
    paddingLeft: 20,
    paddingRight: 20
  },
  buttonText: {
    fontSize: 18,
    color: "white",
    margin: 12
  },

  textInput: {
    padding: 10,
    marginStart: 15,
    fontSize: 15,
    marginEnd: 15
  },
  titleAlign: {
    marginStart: 15,
    marginTop: 15,
    marginBottom: 15,
    color: "#00cccc",
    fontWeight: "bold",
    fontSize: 25
  },
  subTitle: {
    fontSize: 20,
    marginStart: 15,
    color: "#5c5c3d",
    fontWeight: "bold",
    marginBottom: 5
  },
  messageText: {
    fontSize: 17,
    marginStart: 15,
    marginBottom: 25,
    marginEnd: 15
  }
};

const mapStateToProps = state => {
  return {
    //  reduxData: state.todos.data
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      // addTodo: addTodo
    },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(BaseCareEducation);
