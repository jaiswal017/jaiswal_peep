import {
    StackNavigator,
  } from 'react-navigation';

  import Registration from '../container/Registration' 
  import RegistrationForm from '../container/RegistrationForm'
  import Home from '../container/Home'
  import BaseCareEducation from '../container/BaseCareEducation'

  const Navigation = StackNavigator({

    Registration: { screen: Registration },
    RegistrationForm:{ screen : RegistrationForm},
    Home : { screen: Home},
    BaseCareEducation:{screen:BaseCareEducation},
  },
  {
    // see next line
    mode: "none",
    headerMode: "none",
    navigationOptions: {
      gesturesEnabled: false
    }});

  export default Navigation