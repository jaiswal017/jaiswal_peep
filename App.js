import React, { Component } from 'react'
import Navigation from './scr/route/navigation'
import { Provider } from 'react-redux'
import { store } from './scr/store/index'

const App =()=>{
  return(
     <Provider store={store}>
     <Navigation/>
     </Provider>
       )
}
export default App